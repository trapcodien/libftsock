/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   network_select_executions.c                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/01 03:01:36 by garm              #+#    #+#             */
/*   Updated: 2014/12/05 00:15:01 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

void			exec_del(t_exec **head, t_exec *e)
{
	int			status;

	if (head && *head && e)
	{
		waitpid(e->child, &status, WNOHANG);
		if (e->net->exec_return_handle)
			e->net->exec_return_handle(e, WEXITSTATUS(status));
		list_del(head, e, NULL);
	}
}

void			network_select_executions(t_net *net, t_exec *e, int *nrd)
{
	t_exec		*todel;
	ssize_t		rc;

	while (e)
	{
		todel = NULL;
		if (e->pty == ERROR)
			todel = e;
		else if (*nrd > 0 && FD_ISSET(e->pty, &net->read_fds) && (*nrd)--)
		{
			if ((rc = read(e->pty, e->buffer, FTSOCK_EXEC_BUF)) <= 0)
			{
				close(e->pty);
				todel = e;
			}
			else if (net->exec_handle)
			{
				e->buffer[rc] = 0;
				net->exec_handle(e, e->buffer, rc);
			}
		}
		e = e->next;
		if (todel)
			exec_del(&net->executions, todel);
	}
}
