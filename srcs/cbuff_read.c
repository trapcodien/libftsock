/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cbuff_read.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/27 01:50:57 by garm              #+#    #+#             */
/*   Updated: 2014/10/28 03:27:37 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

ssize_t			cbuff_read(int fd, t_cbuff *cbuf, size_t nbytes)
{
	ssize_t		ret;
	size_t		rbytes;
	off_t		*p;
	off_t		*c;

	if (!cbuf)
		return (ERROR);
	if (!nbytes || cbuf->state == CBUFF_FULL)
		return (0);
	p = &(cbuf->off_p);
	c = &(cbuf->off_c);
	rbytes = (*c > *p) ? (*c - *p) : ((off_t)cbuf->size - *p);
	rbytes = (nbytes < rbytes) ? (nbytes) : (rbytes);
	ret = read(fd, ((char *)cbuf->data) + *p, rbytes);
	if (ret <= 0 && rbytes)
		return (ERROR);
	*p += ret;
	if (*p == (off_t)cbuf->size)
		*p = 0;
	cbuf->state = (*p == *c) ? (CBUFF_FULL) : (CBUFF_NORMAL);
	return (ret);
}
