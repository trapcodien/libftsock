/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   network_send.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/28 01:17:05 by garm              #+#    #+#             */
/*   Updated: 2014/11/19 07:45:49 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

static int			is_cbuf(void *data, t_net *net)
{
	t_com			*cursor;
	t_port			*port;

	if (!data || !net)
		return (0);
	port = net->servers;
	while (port)
	{
		cursor = port->clients;
		while (cursor)
		{
			if (cursor->recv.cbuf == data)
				return (1);
			cursor = cursor->next;
		}
		port = port->next;
	}
	port = net->connections;
	while (port)
	{
		if (port->clients->recv.cbuf == data)
			return (1);
		port = port->next;
	}
	return (0);
}

static t_packet		*prepare_packet(void *data, size_t len)
{
	t_packet		*pak;

	pak = (t_packet *)ft_memalloc(sizeof(t_packet));
	if (!pak)
		return (NULL);
	pak->ptr = data;
	pak->written_bytes = 0;
	pak->size = len;
	return (pak);
}

void				network_send(t_com *e, t_com *r, void *data, size_t len)
{
	t_packet		*pak;
	t_diffuse		*dif;
	t_cbuff			*cbuf;

	if (!e || !r || !data || !len)
		return ;
	FD_SET(e->tcps->sock, &e->port->net->write_fds);
	if (is_cbuf(data, e->port->net))
	{
		cbuf = data;
		dif = (t_diffuse *)ft_memalloc(sizeof(t_diffuse));
		dif->offset = (cbuf->off_p - len) % cbuf->size;
		if (dif->offset < 0)
			dif->offset += len;
		dif->len = len;
		dif->cbuf = data;
		e->port->send = utils_queues_push(e->port->send, dif, FTSOCK_DIFFUSE);
		e->port->send->tail->dest = r;
		e->port->send->tail->ack = r->ack;
		return ;
	}
	pak = prepare_packet(data, len);
	e->port->send = utils_queues_push(e->port->send, pak, FTSOCK_PACKET);
	e->port->send->tail->dest = r;
	e->port->send->tail->ack = e->ack;
}
