/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cbuff_get_distance.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/28 02:07:23 by garm              #+#    #+#             */
/*   Updated: 2014/11/20 10:38:25 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

size_t			cbuff_get_distance(t_cbuff *cbuf, size_t src, size_t dst)
{
	size_t		size;

	if (cbuf)
	{
		size = cbuf->size;
		if (dst == src && cbuf->state == CBUFF_FULL)
			return (size);
		else if (dst >= src)
			return ((dst - src));
		else
			return ((size + (dst - src)));
	}
	return (0);
}
