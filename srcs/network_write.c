/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   network_write.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/30 14:50:18 by garm              #+#    #+#             */
/*   Updated: 2014/12/23 00:30:19 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

static int			writting_queue_is_empty(t_cbuff *cbuf, t_com *c)
{
	if (!c->port->send && cbuf)
	{
		cbuf->off_c = cbuf->off_p;
		cbuf->state = CBUFF_EMPTY;
		return (1);
	}
	return (0);
}

static void			recalculate_consummer_offset(t_cbuff *cbuf, t_com *c)
{
	t_queues	*q;
	size_t		flush;
	size_t		dist;
	t_diffuse	*dif;

	if (writting_queue_is_empty(cbuf, c))
		return ;
	q = c->port->send;
	dif = q->data;
	flush = cbuff_get_distance(cbuf, cbuf->off_c, dif->offset);
	while (q)
	{
		if (q->type == FTSOCK_DIFFUSE)
		{
			dif = q->data;
			dist = cbuff_get_distance(cbuf, cbuf->off_c, dif->offset);
			flush = (dist < flush) ? dist : flush;
		}
		q = q->next;
	}
	cbuf->off_c += flush;
	cbuf->off_c %= cbuf->size;
	if (cbuf->off_c == cbuf->off_p)
		cbuf->state = CBUFF_EMPTY;
}

static int			fts_write_packet(t_com *c)
{
	t_packet	*pak;
	size_t		nbytes;
	int			last;
	int			fd_dest;
	ssize_t		ret;

	last = 0;
	pak = c->port->send->data;
	fd_dest = c->port->send->dest->tcps->sock;
	if ((pak->written_bytes + FTSOCK_SEND_BUF) >= pak->size)
	{
		nbytes = pak->size - pak->written_bytes;
		last = 1;
	}
	else
		nbytes = FTSOCK_SEND_BUF;
	ret = write(fd_dest, ((char *)pak->ptr) + pak->written_bytes, nbytes);
	if (ret < 0)
		ret = 0;
	if ((size_t)ret != nbytes)
		last = 0;
	pak->written_bytes += ret;
	return (last);
}

static void			fts_send_packet(t_port *s, t_com *c)
{
	t_packet		*pak;

	if (fts_write_packet(c))
	{
		pak = s->send->data;
		if (s->send->ack && c->ack_handle)
			c->ack_handle(c, pak->ptr, pak->size);
		pak = utils_queues_pop(&(s->send));
		ft_memdel((void **)&pak);
	}
}

void				network_write(t_port *s, t_com *c)
{
	t_diffuse	*dif;
	int			dest_fd;

	if (!s || !c)
		return ;
	if (!(s->send))
		FD_CLR(c->tcps->sock, &(s->net->write_fds));
	if (!(s->send) || !(s->send->dest) || !(s->send->dest->tcps))
		return ;
	if (s->send->type == FTSOCK_DIFFUSE && s->send->dest)
	{
		dif = s->send->data;
		if (s->send->ack && c->ack_handle)
			c->ack_handle(c, dif->cbuf, dif->len);
		dest_fd = s->send->dest->tcps->sock;
		dif = utils_queues_pop(&(s->send));
		cbuff_write_noflush(dest_fd, dif->cbuf, dif->len);
		recalculate_consummer_offset(dif->cbuf, c);
		ft_memdel((void **)&dif);
	}
	else if (s->send->type == FTSOCK_PACKET)
		fts_send_packet(s, c);
	if (!(s->send))
		FD_CLR(c->tcps->sock, &(s->net->write_fds));
}
