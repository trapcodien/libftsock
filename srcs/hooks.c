/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/28 00:59:03 by garm              #+#    #+#             */
/*   Updated: 2014/12/16 00:41:55 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

void	timeout_hook(t_net *s, void *f, t_timeval tv)
{
	if (s)
	{
		s->tv = tv;
		s->timeout_handle = f;
	}
}

void	loop_hook(t_net *s, void *f)
{
	if (s)
		s->loop_handle = f;
}

void	end_loop_hook(t_net *s, void *f)
{
	if (s)
		s->end_loop_handle = f;
}

void	stdin_hook(t_net *s, void *f)
{
	if (s)
		s->stdin_handle = f;
}
