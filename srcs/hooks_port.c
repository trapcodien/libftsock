/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks_port.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/30 19:34:39 by garm              #+#    #+#             */
/*   Updated: 2014/12/14 21:54:33 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

void	connect_hook(t_port *p, void *f, void *com_constructor)
{
	t_com_construct		c;

	c = com_constructor;
	if (p)
	{
		p->connect_handle = f;
		p->com_constructor = c;
		if (!p->tcps && p->clients)
			p->connect_handle(p->clients);
	}
}

void	closeport_hook(t_port *p, void *f)
{
	if (p)
		p->closeport_handle = f;
}
