/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   network_close.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/15 23:58:23 by garm              #+#    #+#             */
/*   Updated: 2014/12/14 21:51:20 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

void			network_close(t_com *com)
{
	if (com)
	{
		com->closed = 1;
		if (com->disconnect_handle)
			com->disconnect_handle(com);
	}
}

void			network_closeall(t_port *port)
{
	t_com		*com;

	if (port)
	{
		com = port->clients;
		while (com)
		{
			network_close(com);
			com = com->next;
		}
	}
}

void			network_closeserver(t_port *port)
{
	if (port && port->tcps)
	{
		if (port->closeport_handle)
			port->closeport_handle(port);
		network_closeall(port);
		port->closed = 1;
	}
}

void			network_exec_close(t_exec *e)
{
	if (e && e->pty != ERROR)
	{
		close(e->pty);
		e->pty = ERROR;
	}
}

void			network_exit(t_net *net)
{
	t_port		*cursor;
	t_exec		*e;

	if (net)
	{
		net->stdin_handle = NULL;
		cursor = net->servers;
		while (cursor)
		{
			network_closeserver(cursor);
			cursor = cursor->next;
		}
		cursor = net->connections;
		while (cursor)
		{
			network_closeall(cursor);
			cursor = cursor->next;
		}
		e = net->executions;
		while (e)
		{
			network_exec_close(e);
			e = e->next;
		}
	}
}
