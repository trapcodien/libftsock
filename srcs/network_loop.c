/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   network_loop.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/28 01:18:02 by garm              #+#    #+#             */
/*   Updated: 2014/12/16 07:37:32 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

static void			select_com(t_net *net, t_com *com, int *nbrd)
{
	ssize_t			rc;

	if (!com->tcps)
		return ;
	if ((*nbrd) > 0 && FD_ISSET(com->tcps->sock, &net->write_fds) && (*nbrd)--)
	{
		network_write(com->port, com);
		return ;
	}
	if ((*nbrd) > 0 && FD_ISSET(com->tcps->sock, &net->read_fds) && (*nbrd)--)
	{
		if (!com->port->send)
		{
			rc = cbuff_read(com->tcps->sock, com->recv.cbuf, FTSOCK_RECV_BUF);
			if (rc == ERROR)
			{
				com->closed = 1;
				if (com->disconnect_handle)
					com->disconnect_handle(com);
			}
			else
				network_receive(com->port, com, com->recv.cbuf, rc);
		}
	}
}

static void			select_new_com(t_net *net, t_port *port, int *nbrd)
{
	t_com			*new;

	if ((*nbrd) > 0 && FD_ISSET(port->tcps->sock, &net->read_fds) && (*nbrd)--)
	{
		if (port->com_constructor)
			new = port->com_constructor();
		else
			new = ft_memalloc(sizeof(t_com));
		if (!new)
			return ;
		new->tcps = ftsock_wait(port->tcps);
		if (!new->tcps || new->tcps->error || new->tcps->sock >= FD_SETSIZE)
		{
			ftsock_destroy(&new->tcps);
			ft_memdel((void **)&new);
			return ;
		}
		new->recv.cbuf = cbuff_create(FTSOCK_MAXBUF);
		new->port = port;
		list_push(&port->clients, new);
		if (port->connect_handle)
			port->connect_handle(new);
	}
}

static void			select_stdin(t_net *net)
{
	ssize_t			rc;

	if (FD_ISSET(STDIN_FILENO, &net->read_fds))
	{
		if ((rc = read(STDIN_FILENO, net->stdin_buffer, FTSOCK_STDIN_BUF)) > 0)
		{
			net->stdin_buffer[rc] = '\0';
			net->stdin_handle(net, net->stdin_buffer, rc);
		}
	}
}

static void			network_select(t_net *net, int nbrd)
{
	t_port			*port;
	t_com			*com;

	port = net->servers;
	while (port)
	{
		select_new_com(net, port, &nbrd);
		com = port->clients;
		while (com)
		{
			select_com(net, com, &nbrd);
			com = com->next;
		}
		port = port->next;
	}
	port = net->connections;
	while (port)
	{
		select_com(net, port->clients, &nbrd);
		port = port->next;
	}
	if (nbrd > 0 && net->stdin_handle)
		select_stdin(net);
	if (nbrd > 0 && net->executions)
		network_select_executions(net, net->executions, &nbrd);
}

void				network_loop(t_net *net)
{
	int				nbrd;
	int				h_fd;
	struct timeval	*tv;

	while (refresh_connections(net) || net->executions)
	{
		refresh_executions(net);
		if (net->loop_handle)
			net->loop_handle(net);
		tv = (net->timeout_handle) ? (&net->tv) : (NULL);
		h_fd = net->higher_fd + 1;
		nbrd = select(h_fd, &net->read_fds, &net->write_fds, NULL, tv);
		if (nbrd < 0)
			network_exit(net);
		if (!nbrd && net->timeout_handle)
			net->timeout_handle(net);
		else
			network_select(net, nbrd);
	}
	if (net && net->end_loop_handle)
		net->end_loop_handle(net);
	if (net && (net->stdin_handle || net->servers || net->connections))
		network_loop(net);
	else
		ft_memdel((void **)&net);
}
