/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   network_foreach.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/15 23:54:32 by garm              #+#    #+#             */
/*   Updated: 2014/11/17 18:22:37 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

static void		network_foreach_com(t_com *com, void *tab_com_f)
{
	t_com			*prev_com;
	t_com_func		cf;
	int				i;

	while (com)
	{
		prev_com = com;
		com = com->next;
		i = 0;
		while (tab_com_f && (cf = *(((t_com_func *)tab_com_f + (i++)))))
			cf(prev_com);
	}
}

void			network_foreach(t_port *port, void *tab_port_f, void *tab_com_f)
{
	t_port_func		pf;
	t_port			*prev_port;
	int				i;

	while (port)
	{
		network_foreach_com(port->clients, tab_com_f);
		i = 0;
		prev_port = port;
		port = port->next;
		while (tab_port_f && (pf = *(((t_port_func *)tab_port_f + (i++)))))
			pf(prev_port);
	}
}
