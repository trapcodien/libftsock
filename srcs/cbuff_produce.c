/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cbuff_produce.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/28 03:11:08 by garm              #+#    #+#             */
/*   Updated: 2014/10/28 14:19:01 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

static void		*utils_ft_memcpy(void *s1, const void *s2, size_t n)
{
	size_t			i;
	char			*str1;
	const char		*str2;

	if (!s1 || !s2)
		return (s1);
	str1 = (char *)s1;
	str2 = (const char *)s2;
	i = 0;
	while (i < n)
	{
		str1[i] = str2[i];
		i++;
	}
	return (s1);
}

ssize_t			cbuff_produce(t_cbuff *cbuf, void *data, size_t size)
{
	size_t		rbytes;
	off_t		*p;
	off_t		*c;

	if (!cbuf)
		return (ERROR);
	if (!size || cbuf->state == CBUFF_FULL)
		return (0);
	p = &(cbuf->off_p);
	c = &(cbuf->off_c);
	rbytes = (*c > *p) ? (*c - *p) : ((off_t)cbuf->size - *p);
	rbytes = (size < rbytes) ? (size) : (rbytes);
	if (!rbytes)
		return (0);
	utils_ft_memcpy(((char *)cbuf->data) + *p, data, rbytes);
	*p += rbytes;
	if (*p == (off_t)cbuf->size)
		*p = 0;
	cbuf->state = (*p == *c) ? (CBUFF_FULL) : (CBUFF_NORMAL);
	return (rbytes + cbuff_produce(cbuf, (char *)data + rbytes, size - rbytes));
}
