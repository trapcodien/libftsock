/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cbuff_findchr.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/28 05:37:44 by garm              #+#    #+#             */
/*   Updated: 2014/11/20 12:35:14 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

ssize_t			cbuff_findchr(t_cbuff *cbuf, char c, off_t offset, size_t len)
{
	size_t		rbytes;
	size_t		i;
	size_t		size;
	char		*data;

	if (!cbuf || !len)
		return (ERROR);
	i = 0;
	size = cbuf->size;
	data = cbuf->data;
	offset = (offset < 0) ? ((offset % size) + size) : (offset % size);
	rbytes = (off_t)size - offset;
	rbytes = (len < rbytes) ? (len) : (rbytes);
	while (i < rbytes)
	{
		if (data[offset + i] == c)
			return (offset + i);
		i++;
	}
	if (len - rbytes)
		return (cbuff_findchr(cbuf, c, 0, (len - rbytes)));
	return (ERROR);
}

static ssize_t	ft_cbuff_findrchr(t_cbuff *cbuf, char c, off_t off, size_t len)
{
	char		*data;
	ssize_t		i;

	if (!len)
		return (ERROR);
	i = len - 1;
	data = cbuf->data;
	while (i >= 0)
	{
		if (data[off + i] == c)
			return (off + i);
		i--;
	}
	return (ERROR);
}

ssize_t			cbuff_findrchr(t_cbuff *cbuf, char c, off_t offset, size_t len)
{
	off_t		decal;
	size_t		size;
	ssize_t		ret;

	if (!cbuf || !len)
		return (ERROR);
	size = cbuf->size;
	if ((offset + len) >= size)
	{
		decal = (offset + len) - size;
		if ((ret = ft_cbuff_findrchr(cbuf, c, 0, decal)) >= 0)
			return (ret);
		decal = decal - len;
		if (decal < 0)
			decal *= -1;
		if ((ret = ft_cbuff_findrchr(cbuf, c, offset, decal)) >= 0)
			return (ret);
	}
	else
		return (ft_cbuff_findrchr(cbuf, c, offset, len));
	return (ERROR);
}
