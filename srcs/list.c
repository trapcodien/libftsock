/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/31 07:57:14 by garm              #+#    #+#             */
/*   Updated: 2014/11/15 21:19:37 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void					*list_create(void *f_construct)
{
	t_list_construct	c;
	void				*list;

	if ((c = f_construct))
		list = c();
	else
		list = ft_memalloc(sizeof(t_list));
	if (!list)
		return (NULL);
	((t_list *)list)->next = NULL;
	return (list);
}

void					list_destroy(void *elem, void *f_destruct)
{
	t_list_destruct		d;
	t_list				**l_elem;

	l_elem = (t_list **)elem;
	if (!l_elem || !*l_elem)
		return ;
	if ((d = f_destruct))
		d(*l_elem);
	ft_memdel(elem);
}

void					list_push(void *list, void *new_elem)
{
	t_list				**l_list;
	t_list				*l_new;

	if (!list || !new_elem)
		return ;
	l_list = (t_list **)list;
	l_new = new_elem;
	l_new->next = *l_list;
	*l_list = l_new;
}

void					list_del(void *list, void *elem, void *f_destruct)
{
	t_list				**l_list;
	t_list				*cursor;
	t_list				*prev;

	l_list = (t_list **)list;
	if (!l_list || !*l_list || !elem)
		return ;
	prev = NULL;
	cursor = *l_list;
	while (cursor && cursor != elem && (prev = cursor))
		cursor = cursor->next;
	if (!cursor)
		return ;
	if (prev)
		prev->next = cursor->next;
	else
		(*l_list) = (*l_list)->next;
	list_destroy((void **)&cursor, f_destruct);
}
