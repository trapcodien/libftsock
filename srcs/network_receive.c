/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   network_receive.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/30 12:22:13 by garm              #+#    #+#             */
/*   Updated: 2015/04/06 00:42:38 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

static void		flush_cbuff(t_cbuff *cbuf)
{
	if (cbuf)
	{
		cbuf->off_c = cbuf->off_p;
		cbuf->state = CBUFF_EMPTY;
	}
}

static void		recv_mode_size(t_com *c, t_cbuff *cbuf, size_t len)
{
	size_t		*size;
	size_t		*rbytes;

	size = &(c->recv.size);
	rbytes = &(c->recv.rbytes);
	while (len > *size)
	{
		if (c->recv_handle)
			c->recv_handle(c, cbuf, *size);
		cbuff_consume(cbuf, NULL, *size);
		len -= *size;
		*rbytes = (len >= *rbytes) ? (len - *rbytes) : (*rbytes - len);
	}
	if (len >= *rbytes)
	{
		if (c->recv_handle)
			c->recv_handle(c, cbuf, *size);
		cbuff_consume(cbuf, NULL, *size);
		*rbytes = (len >= *rbytes) ? (len - *rbytes) : (*rbytes - len);
	}
	else
		*rbytes -= len;
	if (!*rbytes)
		*rbytes = *size;
}

static void		recv_recall(t_port *s, t_com *c, t_cbuff *cbuf, size_t dist)
{
	ssize_t		chr;
	int			recall;

	recall = 0;
	if (s->send)
		return ;
	if (c->recv.mode == MODE_RAW && dist > 0)
		recall = 1;
	else if (c->recv.mode == MODE_SIZE && dist >= c->recv.rbytes)
		recall = 1;
	else if (c->recv.mode == MODE_TEXT)
	{
		if ((chr = cbuff_findrchr(cbuf, '\n', cbuf->off_c, dist)) >= 0)
			recall = 1;
	}
	if (recall && c->recv_handle)
		network_receive(s, c, cbuf, dist + 1);
}

void			network_receive(t_port *s, t_com *c, t_cbuff *cbuf, size_t len)
{
	ssize_t		chr;
	size_t		dist;
	size_t		flush;

	flush = 0;
	dist = cbuff_get_distance(cbuf, cbuf->off_c, cbuf->off_p);
	if (c->recv.mode == MODE_TEXT)
	{
		if ((chr = cbuff_findchr(cbuf, '\n', cbuf->off_c, dist)) >= 0)
		{
			if (c->recv_handle)
			{
				dist = cbuff_get_distance(cbuf, cbuf->off_c, chr + 1);
				flush = c->recv_handle(c, cbuf, dist);
			}
		}
	}
	else if (c->recv.mode == MODE_RAW && c->recv_handle)
		flush = c->recv_handle(c, cbuf, dist);
	else if (c->recv.mode == MODE_SIZE)
		recv_mode_size(c, cbuf, len);
	cbuff_consume(cbuf, NULL, flush);
	if (cbuf->state == CBUFF_FULL && s && !s->send)
		flush_cbuff(cbuf);
	recv_recall(s, c, cbuf, cbuff_get_distance(cbuf, cbuf->off_c, cbuf->off_p));
}
