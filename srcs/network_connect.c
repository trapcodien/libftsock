/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   network_connect.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/12 07:09:16 by garm              #+#    #+#             */
/*   Updated: 2014/11/29 07:43:31 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

t_com		*network_free_port_com(t_port *port, t_com *com)
{
	if (port)
		ft_memdel((void **)&port);
	if (com)
	{
		if (com->tcps)
			ftsock_destroy(&com->tcps);
		ft_memdel((void **)&com);
	}
	return (NULL);
}

t_com		*network_connect(t_net *net, char *host, int port, void *construct)
{
	t_com				*com;
	t_port				*connection;
	t_com_construct		com_construct;

	if (!net || !(connection = ft_memalloc(sizeof(t_port))))
		return (NULL);
	if ((com_construct = construct))
		com = com_construct();
	else
		com = ft_memalloc(sizeof(t_com));
	if (!com)
		return (network_free_port_com(connection, com));
	if (!(com->tcps = ftsock_create(FTSOCK_CLIENT, host, port)))
		return (network_free_port_com(connection, com));
	ftsock_connect(com->tcps);
	if (com->tcps->error || com->tcps->sock >= FD_SETSIZE)
		return (network_free_port_com(connection, com));
	connection->net = net;
	com->port = connection;
	com->recv.cbuf = cbuff_create(FTSOCK_MAXBUF);
	list_push(&(connection->clients), com);
	list_push(&(net->connections), connection);
	return (com);
}
