/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cbuff_consume.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/28 03:18:24 by garm              #+#    #+#             */
/*   Updated: 2014/12/16 09:55:08 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

static void		*utils_ft_memcpy(void *s1, const void *s2, size_t n)
{
	size_t			i;
	char			*str1;
	const char		*str2;

	if (!s1 || !s2)
		return (s1);
	str1 = (char *)s1;
	str2 = (const char *)s2;
	i = 0;
	while (i < n)
	{
		str1[i] = str2[i];
		i++;
	}
	return (s1);
}

ssize_t			cbuff_consume(t_cbuff *cbuf, void *data, size_t size)
{
	size_t		rbytes;
	size_t		new_size;
	off_t		*p;
	off_t		*c;

	if (!cbuf)
		return (ERROR);
	if (!size || cbuf->state == CBUFF_EMPTY)
		return (0);
	p = &(cbuf->off_p);
	c = &(cbuf->off_c);
	rbytes = (*p > *c) ? (*p - *c) : ((off_t)cbuf->size - *c);
	rbytes = (size < rbytes) ? (size) : (rbytes);
	if (!rbytes)
		return (0);
	utils_ft_memcpy(data, ((char *)cbuf->data) + *c, rbytes);
	*c += rbytes;
	if (*c == (off_t)cbuf->size)
		*c = 0;
	cbuf->state = (*c == *p) ? (CBUFF_EMPTY) : (CBUFF_NORMAL);
	new_size = size - rbytes;
	if (data)
		return (rbytes + cbuff_consume(cbuf, (char *)data + rbytes, new_size));
	else
		return (rbytes + cbuff_consume(cbuf, data, new_size));
}

ssize_t			cbuff_consume_noflush(t_cbuff *cbuf, void *data, size_t size)
{
	off_t		old_consummer_offset;
	char		old_state;
	ssize_t		ret;

	if (!cbuf)
		return (ERROR);
	old_consummer_offset = cbuf->off_c;
	old_state = cbuf->state;
	ret = cbuff_consume(cbuf, data, size);
	cbuf->state = old_state;
	cbuf->off_c = old_consummer_offset;
	return (ret);
}
