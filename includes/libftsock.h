/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftsock.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/05/12 16:29:18 by garm              #+#    #+#             */
/*   Updated: 2014/12/16 00:42:38 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTSOCK_H
# define LIBFTSOCK_H

# include <util.h>
# include <unistd.h>
# include <stdarg.h>
# include <stdlib.h>
# include <sys/select.h>
# include <signal.h>

# include "libft.h"
# include "libftsock_primitives.h"

# define MAX_PORT 65535

# define FTSOCK_MAXBUF ((1024) * (1024) * (1))
# define FTSOCK_BUF (4096)
# define FTSOCK_RECV_BUF FTSOCK_BUF
# define FTSOCK_SEND_BUF FTSOCK_BUF
# define FTSOCK_STDIN_BUF FTSOCK_BUF
# define FTSOCK_EXEC_BUF FTSOCK_BUF

# define MODE_RAW 0
# define MODE_TEXT 1
# define MODE_SIZE 2
# define DEFAULT_MODE_SIZE (FTSOCK_BUF)

# define FTSOCK_NO_ACK 0
# define FTSOCK_WITH_ACK 1

# include "list.h"
# include "libftsock_core.h"

/*
** --- Events ---
*/
void		timeout_hook(t_net *n, void *func, t_timeval tv);
void		loop_hook(t_net *n, void *func);
void		end_loop_hook(t_net *n, void *func);
void		stdin_hook(t_net *n, void *func);
void		exec_hook(t_net *n, void *func, void *exec_constructor);
void		exec_return_hook(t_net *n, void *func);

void		connect_hook(t_port *p, void *func, void *com_constructor);
void		closeport_hook(t_port *p, void *f);

void		disconnect_hook(t_com *c, void *func);
void		recv_hook(t_com *c, void *func);
void		ack_hook(t_com *c, void *func);

/*
** --- Network ---
*/
t_net		*network_create(void *construct);
t_port		*network_listen(t_net *net, int port, int backlog, void *construct);
t_com		*network_connect(t_net *net, char *host, int port, void *construct);
void		network_loop(t_net *net);
void		network_close(t_com *com);
void		network_closeall(t_port *port);
void		network_closeserver(t_port *port);
void		network_exit(t_net *net);

/*
** --- Network Utilities ---
*/
void		network_foreach(t_port *port, void *tab_port_f, void *tab_com_f);
int			network_is_server(t_port *port);

/*
** --- Communication ---
*/
void		network_send(t_com *e, t_com *r, void *data, size_t len);
void		network_recv_mode(t_com *c, char mode);
void		network_recv_mode_setsize(t_com *c, size_t size);

/*
** --- Execution ---
*/
t_exec		*network_exec(t_net *net, char *path, char **argv, char **env);
ssize_t		network_exec_write(t_exec *e, void *buffer, size_t size);
void		network_exec_close(t_exec *e);

/*
** --- PRIMITIVES SOCKET FUNCTIONS ---
** ftsock.c
*/
t_sock		*ftsock_create(char type, ...);
void		ftsock_destroy(t_sock **socket);

/*
** ftsock_client.c
*/
t_sock		*ftsock_create_client(char *ip, int port);
void		ftsock_connect(t_sock *s);

/*
** ftsock_server.c
*/
t_sock		*ftsock_create_server(int port, int queue_len);
t_sock		*ftsock_wait(t_sock *serv);

/*
** ftsock_resolve.c
*/
void		ftsock_resolve_host(t_sock *host, char *ip);

/*
** ftsock_error.c
*/
int			ftsock_perror(char error);

/*
** --- internal use only ---
*/
void		network_receive(t_port *s, t_com *c, t_cbuff *cbuf, size_t len);
void		network_write(t_port *s, t_com *c);
void		close_com(t_com *com);
void		close_port(t_port *port);
void		refresh_com_higher_fd(t_com *com);
void		refresh_port_higher_fd(t_port *port);
int			refresh_connections(t_net *net);
int			refresh_executions(t_net *net);
void		exec_del(t_exec **head, t_exec *e);
void		network_select_executions(t_net *net, t_exec *e, int *nbrd);
t_queues	*utils_queues_create(void *data, char type);
t_queues	*utils_queues_push(t_queues *q, void *data, char type);
void		*utils_queues_pop(t_queues **q);
t_cbuff		*cbuff_create(size_t size);
void		cbuff_destroy(t_cbuff **b);
size_t		cbuff_get_distance(t_cbuff *cbuf, size_t src, size_t dst);
ssize_t		cbuff_read(int fd, t_cbuff *cbuf, size_t nbytes);
ssize_t		cbuff_produce(t_cbuff *cbuf, void *data, size_t size);
ssize_t		cbuff_write(int fd, t_cbuff *cbuf, size_t nbytes);
ssize_t		cbuff_write_noflush(int fd, t_cbuff *cbuf, size_t nbytes);
ssize_t		cbuff_consume(t_cbuff *cbuf, void *data, size_t size);
ssize_t		cbuff_consume_noflush(t_cbuff *cbuf, void *data, size_t size);
ssize_t		cbuff_findchr(t_cbuff *cbuf, char c, off_t offset, size_t len);
ssize_t		cbuff_findrchr(t_cbuff *cbuf, char c, off_t offset, size_t len);
int			cbuff_debug(t_cbuff *cbuf);

#endif
