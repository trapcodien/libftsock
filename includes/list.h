/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/31 22:28:42 by garm              #+#    #+#             */
/*   Updated: 2014/11/15 21:19:36 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIST_H
# define LIST_H

# include "libft.h"

typedef struct		s_list
{
	struct s_list	*next;
}					t_list;

typedef void		*(*t_list_construct)(void);
typedef void		(*t_list_destruct)(void *);

void				*list_create(void *f_construct);
void				list_destroy(void *elem, void *f_destruct);
void				list_push(void *list, void *new_elem);
void				list_del(void *list, void *elem, void *f_destruct);

#endif
