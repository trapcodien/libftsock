# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: garm <garm@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2014/11/29 01:59:17 by garm              #+#    #+#              #
#    Updated: 2014/12/01 03:01:21 by garm             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc

NAME = libftsock.a
LIBS = -lft
FTLIBS = libft.a

SOURCES_DIR = srcs
INCLUDES_DIR = includes

CHECK = @cppcheck -I $(INCLUDES_DIR) --enable=all --check-config --suppress=missingIncludeSystem .

ifeq ($(DEBUG), 1)
	FLAGS = -g -Wall -Wextra
	CC = cc
else
	FLAGS = -Wall -Werror -Wextra -ansi -std=c89
endif

CFLAGS = $(FLAGS) -I $(INCLUDES_DIR)

DEPENDENCIES = \
			   $(INCLUDES_DIR)/libftsock.h \
			   $(INCLUDES_DIR)/libftsock_core.h \
			   \

SOURCES = \
			 $(SOURCES_DIR)/ftsock.c \
			 $(SOURCES_DIR)/ftsock_client.c \
			 $(SOURCES_DIR)/ftsock_server.c \
			 $(SOURCES_DIR)/ftsock_resolve.c \
			 $(SOURCES_DIR)/ftsock_error.c \
			 $(SOURCES_DIR)/network_create.c \
			 $(SOURCES_DIR)/network_listen.c \
			 $(SOURCES_DIR)/network_connect.c \
			 $(SOURCES_DIR)/network_loop.c \
			 $(SOURCES_DIR)/network_foreach.c \
			 $(SOURCES_DIR)/network_utils.c \
			 $(SOURCES_DIR)/network_refresh.c \
			 $(SOURCES_DIR)/network_close.c \
			 $(SOURCES_DIR)/network_receive.c \
			 $(SOURCES_DIR)/network_write.c \
			 $(SOURCES_DIR)/network_send.c \
			 $(SOURCES_DIR)/network_exec.c \
			 $(SOURCES_DIR)/network_select_executions.c \
			 $(SOURCES_DIR)/hooks.c \
			 $(SOURCES_DIR)/hooks_com.c \
			 $(SOURCES_DIR)/hooks_port.c \
			 $(SOURCES_DIR)/hooks_exec.c \
			 $(SOURCES_DIR)/network_recv_mode.c \
			 $(SOURCES_DIR)/utils_queues_create.c \
			 $(SOURCES_DIR)/utils_queues_push.c \
			 $(SOURCES_DIR)/utils_queues_pop.c \
			 $(SOURCES_DIR)/cbuff_create.c \
			 $(SOURCES_DIR)/cbuff_destroy.c \
			 $(SOURCES_DIR)/cbuff_read.c \
			 $(SOURCES_DIR)/cbuff_produce.c \
			 $(SOURCES_DIR)/cbuff_write.c \
			 $(SOURCES_DIR)/cbuff_consume.c \
			 $(SOURCES_DIR)/cbuff_get_distance.c \
			 $(SOURCES_DIR)/cbuff_findchr.c \
			 $(SOURCES_DIR)/cbuff_debug.c \
			 $(SOURCES_DIR)/list.c \

OBJS = $(SOURCES:.c=.o)

all: $(NAME)

%.o: %.c $(DEPENDENCIES)
	$(CC) -c $< -o $@ $(CFLAGS)

$(NAME): $(OBJS)
	@echo Creating $(NAME)...
	@ar rcs $(NAME) $(OBJS)

clean:
	@rm -f $(OBJS)
	@echo Deleting $(NAME) OBJECTS files...

cleanbin:
	@rm -f $(NAME)
	@echo Deleting $(NAME)...

check:
	$(CHECK)

fclean: clean cleanbin
	@echo Full clean OK.

re: fclean all

.PHONY: clean cleanbin fclean re all test

